Rails.application.routes.draw do


  devise_for :developers


  root 'estates#index'

  resources :developers do
    resources :estates
  end


  get 'question/search', to: 'customer#search', as: 'search'
  post 'question/find', to: 'customer#find', as: 'find'
  get 'question/find', to: 'customer#find', as: 'find_get'
  get 'sort', to: 'customer#sort_by', as: 'sort_by'
  post 'sort', to: 'customer#sort', as: 'sort'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
