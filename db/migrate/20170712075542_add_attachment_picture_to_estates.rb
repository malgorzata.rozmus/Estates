class AddAttachmentPictureToEstates < ActiveRecord::Migration
  def self.up
    change_table :estates do |t|
      t.attachment :picture
    end
  end

  def self.down
    remove_attachment :estates, :picture
  end
end
