class CreateDevelopers < ActiveRecord::Migration[5.0]
  def change
    create_table :developers do |t|
      t.string :company
      t.string :name
      t.integer :phone_nr
      t.integer :estate_id

      t.timestamps
    end
  end
end
