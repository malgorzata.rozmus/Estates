class CreateEstates < ActiveRecord::Migration[5.0]
  def change
    create_table :estates do |t|
      t.string :type
      t.float :surface
      t.integer :room
      t.integer :bathroom
      t.boolean :garage
      t.string :city
      t.string :street
      t.float :price
      t.string :photo

      t.timestamps
    end
  end
end
