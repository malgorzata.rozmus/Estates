class AddDeveloperIdToEstates < ActiveRecord::Migration[5.0]
  def change
    add_column :estates, :developer_id, :integer
  end
end
