class ChangeColumnName < ActiveRecord::Migration[5.0]
  def change
    change_table :estates do |t|
      t.rename :type, :estate_type
      end
  end
end
