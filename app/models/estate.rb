class Estate < ApplicationRecord


  validates :estate_type, :surface, :room, :bathroom, :price, :city , :street, presence: true
  validates_numericality_of :room, :bathroom, :only_integer => true, :message => "can only be whole number.", :allow_nil => true
  validates_numericality_of :surface, :price, :only_float => true, :message => "can only be whole number.", :allow_nil => true


  belongs_to :developer

  geocoded_by :full_address
  after_validation :geocode

  def full_address
    [city,street].compact.join(',')
  end





  has_attached_file :picture, styles: { medium: "300x300>", thumb: "100x100>" }
  validates_attachment_content_type :picture, content_type: /\Aimage\/.*\z/
end
