class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :configure_permitted_parameters, if: :devise_controller?

  layout :set_layout
  def set_layout
    if current_developer
      'developer'
    else
      'estate'
    end
  end
protected
  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:company, :name, :phone_nr])
  end
  private
  def after_sign_in_path_for(current_developer)
    new_developer_estate_path(:developer_id => current_developer.id)
  end
end
