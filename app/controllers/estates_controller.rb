class EstatesController < ApplicationController

  def index


    @estates = Estate

    if params[:sort].present?
      result_table=['developers.company', 'price', 'surface']
      if result_table.include?(params[:sort])
        @estates=@estates.joins(:developer).order(params[:sort])
      end

    end
    @estates = @estates.all
    @hash=Gmaps4rails.build_markers(@estates) do |place, marker|
      marker.lat place.latitude
      marker.lng place.longitude
    end

  end


  def new
    @developer=Developer.find_by(:id => current_developer.id)
    @estate=@developer.estates.build;

  end

  def create
    @developer=Developer.find_by(:id => current_developer.id)
    @estate=@developer.estates.create(estate_params)

    if @estate.save
      redirect_to root_url
    else
      render 'new'
    end

  end

  def edit
    @developer=Developer.find_by(:id => params[:developer_id])
    @estate=@developer.estates.find(params[:id])
  end

  def update
    @developer=Developer.find_by(:id => params[:developer_id])
    @estate=@developer.estates.find(params[:id])
    @estate.update(estate_params)
    redirect_to root_url
  end


  private
  def estate_params
    params.require(:estate).permit(:estate_type, :surface, :room, :bathroom, :garage, :city, :street, :price, :picture)
  end


  def correct_developer
    @developer=current_developer.estates.find_by(:id => params[:id])
    redirect_to developer_estates_path if @developer.nil?
  end
end
