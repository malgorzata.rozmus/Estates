class CustomerController < ApplicationController


  def search

  end

  def find
    @developer=params[:developer]
    if !@developer
      @developer=current_developer.company
    end
    @estates=Estate.joins(:developer).where('developers.company=?', @developer)
    @estates=@estates.where('estate_type =?', params[:estate_type]) if params[:estate_type].present?

    @estates=@estates.where('surface>= ?', params[:surface_from]) if params[:surface_from].present?
    @estates=@estates.where('surface<= ?', params[:surface_to]) if params[:surface_to].present?
    @estates=@estates.where('room>= ?', params[:room_min]) if params[:room_min].present?
    @estates=@estates.where('room<= ?', params[:room_max]) if params[:room_max].present?
    @estates=@estates.where('city=?', params[:city]) if params[:city].present?
    if params[:garage].present?
      @garage=true
      @estates=@estates.where('garage=?', @garage)
    end

    @estates=@estates.order(params[:sort]) if params[:sort].present?

    @estates_localization=@estates
    @hash=Gmaps4rails.build_markers(@estates_localization) do |place, marker|
      marker.lat place.latitude
      marker.lng place.longitude
    end


  end



end
