class DevelopersController < ApplicationController





  def index
    @developers=Developer.all
  end


  def edit
    @developer=Developer.find(params[:id])
  end


  def update
    @developer=Developer.find(params[:id])
     if @developer.update(dev_params)
      redirect_to root_url
     else
       render 'edit'
       end

  end



  private
  def dev_params
    params.require(:developer).permit(:company, :name, :phone_nr)
  end



end
